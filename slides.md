---
layout: cover
class: text-center
---

# Einführung in die Programmierung

---

# Michael Buchholz

<div class="leading-8 opacity-80">
Trainer und freiberuflicher Entwickler.<br>
Open Source Enthusiast.<br>
From Backend to Frontend (Web).<br>
</div>

<div class="my-10 grid grid-cols-[40px,1fr] w-max gap-y-4">
  <ri-github-line class="opacity-50"/>
  <div><a href="https://github.com/lean-stack" target="_blank">lean-stack</a></div>
</div>

<img src="/profile.jpg" class="rounded-full w-40 abs-tr mt-16 mr-12"/>

---
layout: cover
---

# Einführung in die Programmierung

Spezielle Betrachtung des Themas Programmierung unter dem Gesichtspunkt
einer gelungenen Eheführung.

---

# Zu meiner Person

<v-clicks>

- Professionelle Erfahrungen als Programmierer:
- 30 Jahre
- Erfolgreich als Ehemann:
- 14 Tage

</v-clicks>

---

# Programmierparadigmen

Paradigmen beschreiben die Art und Weise der Programmierung

<v-clicks>

- Imperative Programmierung
- Strukturierte Programmierung
- Objektorientierte Programmierung
- Funktionale Programmierung
- Logik-basierte Programmierung

</v-clicks>

---

# Imperative Programmierung 

